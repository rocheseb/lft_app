from setuptools import setup

setup(
    name="lft_app",
    description="Make plots from Linefit 14.8 outputs",
    author="Sebastien Roche",
    author_email="sebastien.roche@mail.utoronto.ca",
    version="1.0.0",
    url="https://bitbucket.org/rocheseb/lft_app",
    license="MIT",
    packages=["lft_app"],
    package_dir={"lft_app": "lft_app"},
    package_data={
        "lft_app": [
            "*.inp",
            "static/*.html",
            "spectra/cut/*.dpt",
            "templates/*",
        ]
    },
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "lft_app=lft_app.main:main",
        ],
    },
    zip_safe=False,
    install_requires=[
        "numpy",
        "pandas",
        "netcdf4",
        "toml",
        "tornado==4.5.2",
        "parse",
        "regex",
        "jinja2==2.9.6",
        "matplotlib==3.5.3",
        "bokeh==1.2.0",
        "markupsafe==2.0.1",
    ],
    python_requires=">=3.7",
)
